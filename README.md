Parses RGB Hex Strings into RGB structs and prints the contents.  This is my solution to Tim Clicks' coding challenge posted on 07/22/2021.

## Potential Features
- [x] Command Line Interface with clap
- [x] Error handling
- [ ] lib.rs file as an interface
- [ ] fancy printout with more info
- [ ] documentation
- [ ] add color to the command line
- [ ] hsl conversion?
- [ ] multiple types of inputs
- [ ] color correction (because I guess jpeg does weird stuff)
- [ ] gui interface

## Note
The # needs to be escaped with \ or "" for the command line interface.
