mod cli;
mod rgb;
mod rgb_error;

use cli::Cli;
use rgb_error::RgbError;

fn main() -> Result<(), RgbError> {
    Cli::new().parse()
}
