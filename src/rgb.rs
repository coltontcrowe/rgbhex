use crate::rgb_error::RgbError;
use std::convert::TryFrom;
use std::fmt;

#[derive(Default, Copy, Clone, PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub struct Rgb {
    r: u8,
    g: u8,
    b: u8,
}

impl Rgb {
    fn new(r: u8, g: u8, b: u8) -> Self {
        Self {
            r,
            g,
            b
        }
    }
}

impl fmt::Display for Rgb {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "#{:X}{:X}{:X}", self.r, self.g, self.b)
    }
}

impl TryFrom<&str> for Rgb {
    type Error = RgbError;
    fn try_from(hexcode: &str) -> Result<Self, Self::Error> {
        let stringcode = hexcode.trim();

        let digits = stringcode.strip_prefix("#").ok_or(RgbError::NoLeadingHash)?;
        let parsed = digits.as_bytes()
            .chunks(2)
            .map(std::str::from_utf8)
            .map(|digit| u8::from_str_radix(digit.unwrap(), 16))
            .collect::<Result<Vec<u8>, _>>()?;
        let len = parsed.len();
        

        match len {
            3 => Ok(Rgb::new(parsed[0], parsed[1], parsed[2])),
            0..=2 => Err(RgbError::InputLengthTooSmall(len)),
            _ => Err(RgbError::InputLengthTooLarge(len)),
        }
    }
}