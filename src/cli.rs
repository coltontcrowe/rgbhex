use crate::rgb::Rgb;
use crate::rgb_error::RgbError;
use clap::{App};
use std::convert::TryFrom;

pub struct Cli<'a> {
    app: App<'a>,
}

impl<'a> Cli<'a> {
    pub fn new() -> Self {
        let app = App::new("RGB Hex")
            .version("0.0.0")
            .author("Colton Crowe <coltontcrowe@gmail.com>")
            .about("Parses Hex RGB Strings into their components")
            .arg("<hexcodes>... 'A sequence of various Hex RGB codes, i.e. #8a2be2 #AFEEEE #808080'");
        
        Self {
            app,
        }
    }
    pub fn parse(&self) -> Result<(), RgbError> {
        for hexcode in self.hexcodes()? {
            let rgb = Rgb::try_from(hexcode.as_ref());
            println!("{:#?}", rgb);
        }
        Ok(())
    }

    fn hexcodes(&self) -> Result<Vec<String>, RgbError> {
        let codes = self.app.clone().get_matches()
            .values_of_t::<String>("hexcodes")?;
        Ok(codes)
    }

}