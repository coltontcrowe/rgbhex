use std::num::ParseIntError;
use std::error;
use std::fmt;

#[derive(Debug)]
pub enum RgbError {
    NoLeadingHash,
    InvalidByte(ParseIntError),
    InputLengthTooSmall(usize),
    InputLengthTooLarge(usize),
    CommandLineParsing(clap::Error)
}

impl fmt::Display for RgbError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let msg = match self {
            Self::NoLeadingHash => format!("Missing Hash # at start"),
            Self::InvalidByte(e) => format!("Invalid character: {}", e),
            Self::InputLengthTooSmall(n) => format!("Input length too small. 3 bytes needed. {} were provided.", n),
            Self::InputLengthTooLarge(n) => format!("Input length too large. 3 bytes needed. {} were provided.", n),
            Self::CommandLineParsing(e) => format!("Command Line Parsing Error: {}", e),
        };
        write!(f, "RGB Parsing Error: {}", msg)
    }
}

impl error::Error for RgbError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Self::InvalidByte(e) => Some(e),
            Self::CommandLineParsing(e) => Some(e),
            _ => None,
        }
    }
}

impl From<ParseIntError> for RgbError {
    fn from(err: ParseIntError) -> Self {
        Self::InvalidByte(err)
    }
}

impl From<clap::Error> for RgbError {
    fn from(err: clap::Error) -> Self {
        Self::CommandLineParsing(err)
    }
}